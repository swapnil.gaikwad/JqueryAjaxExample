// Simple way of doing callback

// var x = function(){
//     console.log("I am called from inside a function");
// };

// var y = function(callback){
//     console.log('do Something');
//     callback();
// };

// y(x);

var add = function(a,b){
    return a + b;
};

var multiply = function(a,b){
    return a * b;
};

var doWhatever = function(a,b){
    console.log(`here are your two numbers back ${a} , ${b}`);
};

var calc = function(num1, num2, callback){
    if(typeof callback === 'function'){
        return callback(num1 , num2);
    }
};

console.log(calc(2,3,doWhatever));

/* Use of CallBack function */

var myArr = [{
    num : 5,
    str: 'apple'
},{
    num : 7,
    str: 'cabbage'
},{
    num: 1,
    str: 'ban'
}];

myArr.sort(function(val1 ,val2){
    if(val1.num < val2.num ){
        return -1;
    }else{
        return 1;
    }
});

console.log(myArr);