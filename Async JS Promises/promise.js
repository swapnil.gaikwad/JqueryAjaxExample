//  var promiseMadeByMan = new Promise(function(resolve , reject){

//     var man = false;

//     if(man){
//         resolve('Completed');
//     }else{
//         reject('not Completed');
//     }
//  });

//  promiseMadeByMan.then(function(fromResolve){
//      console.log("Promise made by Man is "+ fromResolve);
//  }).catch(function(fromReject){
//     console.log("Promise made By Man is "+ fromReject);
//  });


var cleanCode = function(){
    return new Promise(function(resolve, reject){
        resolve("Please write Clean Code");
    });
};

var garbageCode = function(message){
    return new Promise(function(resolve, reject){
        resolve(message + "please Remove Garbage Code");
    });
};

var winPingDomRating = function(message){
    return new Promise(function(resolve, reject){
        resolve(message + "WON PINGDOM Report");
    });
};


// cleanCode().then(function(results){
//     return garbageCode(results);
// }).then(function(results){
//     return winPingDomRating(results);
// }).then(function(results){
//     console.log('Finished' + results);
// });


// Promise.all([cleanCode() , garbageCode(), winPingDomRating()]).then(function(){
//     console.log("Finished all Call at Same time");
// });


Promise.race([cleanCode(), garbageCode(), winPingDomRating()]).then(function(){
    console.log("Any one function is called");
})